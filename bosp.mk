
ifdef CONFIG_EXTERNAL_LOG4CPP

# Targets provided by this project
.PHONY: log4cpp clean_log4cpp

# Add this to the "external" target
external: log4cpp
clean_external: clean_log4cpp
distclean_external: distclean_log4cpp

MODULE_DIR_LOG4CPP=$(BASE_DIR)/external/required/log4cpp
BUILD_DIR_LOG4CPP=$(MODULE_DIR_LOG4CPP)/build/
VERSION_LOG4CPP:=`cat $(BUILD_DIR_LOG4CPP)/log4cpp.spec | grep Version | awk '{ print $2 }'`

log4cpp: setup $(BUILD_DIR_LOG4CPP)/Makefile
	@echo
	@echo "==== Building Log4CPP library $(VERSION_LOG4CPP) ===="
	@cd $(BUILD_DIR_LOG4CPP) && \
		make -j$(CPUS) install


$(MODULE_DIR_LOG4CPP)/configure:
	@echo
	@echo "==== Auto-Configure Log4CPP library $(VERSION_LOG4CPP) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@cd $(MODULE_DIR_LOG4CPP) && autoreconf -fi


$(BUILD_DIR_LOG4CPP)/Makefile: $(MODULE_DIR_LOG4CPP)/configure
	@echo
	@echo "==== Configuring Log4CPP library $(VERSION_LOG4CPP) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	[ -d $(BUILD_DIR_LOG4CPP) ] || \
		mkdir -p $(BUILD_DIR_LOG4CPP)
	@cd $(BUILD_DIR_LOG4CPP) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		$(MODULE_DIR_LOG4CPP)/configure \
		--host=$(PLATFORM_TARGET) \
		--prefix=$(BOSP_SYSROOT)/usr \
		--with-pic \
		--with-gnu-ld --disable-dependency-tracking \
		--enable-shared

clean_log4cpp:
	@echo
	@echo "==== Clean-up Log4CPP library $(VERSION_LOG4CPP) ===="
	@[ -d $(BUILD_DIR_LOG4CPP) ] && cd $(BUILD_DIR_LOG4CPP) && make clean || exit 0
	@echo

distclean_log4cpp:
	@echo
	@echo "==== Dist-Cleanup Log4CPP library $(VERSION_LOG4CPP) ===="
	@[ -d $(BUILD_DIR_LOG4CPP) ] && cd $(BUILD_DIR_LOG4CPP) && make distclean || exit 0
	@echo

else # CONFIG_EXTERNAL_LOG4CPP

log4cpp:
	$(warning $(MODULE_DIR_LOG4CPP) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_LOG4CPP

